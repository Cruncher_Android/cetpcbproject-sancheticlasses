import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuestionPanelPageRoutingModule } from './question-panel-routing.module';

import { QuestionPanelPage } from './question-panel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuestionPanelPageRoutingModule
  ],
  declarations: [QuestionPanelPage]
})
export class QuestionPanelPageModule {}
