import { Component, OnInit, Input } from "@angular/core";
import { PopoverController } from "@ionic/angular";
import { ToastServiceService } from "./../../services/toast-service.service";
import { ApiServiceService } from "./../../services/api-service.service";
import { HttpClient } from "@angular/common/http";
import { AlertServiceService } from "src/app/services/alert-service.service";
import { HTTP } from "@ionic-native/http/ngx";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"],
})
export class ForgotPasswordComponent implements OnInit {
  @Input() emailId: string = "";
  @Input() distributerId: string = "";
  @Input() appId: string = "";
  contactNo: string = "7768046436";
  contact: string = "******" + this.contactNo.substr(6, 4);
  userEnteredOTP: string = "";
  OTP: number;
  OTPMatched: boolean;
  reTypePass: string = "";
  passwordMatch: boolean;
  password: string = "";
  resendOTP: boolean = false;
  passInputType: string = "password";
  passIconName: string = "eye";
  reTypePassInputType: string = "password";
  reTypePassIconName: string = "eye";

  constructor(
    private http: HttpClient,
    private nativeHttp: HTTP,
    private popoverController: PopoverController,
    private toastService: ToastServiceService,
    private apiService: ApiServiceService,
    private alertService: AlertServiceService
  ) {}

  ngOnInit() {
    // this.OTP = Math.floor((Math.random() * 10000)).toString();
  }

  onVerifyOTP() {
    if (this.OTP == parseInt(this.userEnteredOTP)) {
      this.OTPMatched = true;
      this.http
        .post(`${this.apiService.apiUrl}forgotPasswordUpdated`, {
          email: this.emailId,
          password: this.password,
          distributerId: this.distributerId,
          appId: this.appId,
        })
        .subscribe(
          (data) => {
            if (data) {
              this.toastService
                .createToast("password Updated")
                .then((_) => this.popoverController.dismiss());
            }
          },
          (err) => {
            this.http
              .post(`${this.apiService.apiUrl}forgotPasswordUpdated`, {
                email: this.emailId,
                password: this.password,
                distributerId: this.distributerId,
                appId: "1",
              })
              .subscribe(
                (data) => {
                  if (data) {
                    this.toastService
                      .createToast("password Updated")
                      .then((_) => this.popoverController.dismiss());
                  }
                },
                (err) => {
                  this.toastService.createToast(
                    "Error while updating password"
                  );
                }
              );
          }
        );
    } else {
      this.OTPMatched = false;
    }
  }

  onChangePassClick() {
    if (this.password == this.reTypePass) {
      this.passwordMatch = true;
      this.OTP = Math.floor(1000 + Math.random() * 9000);
      // this.alertService.createAlert(`Use ${this.OTP} code as a OTP`);
      this.toastService
        .createToast("Sending OTP check your mail...")
        .then((_) => {
          this.sendOTPThroughMail(this.emailId, this.OTP).then(
            (data) => {
              console.log("data", data.data);
              this.toastService.createToast("OTP sent successfully");
            },
            (err) => this.toastService.createToast("Failed to send OTP")
          );
        });
    } else this.passwordMatch = false;
  }

  sendOTPThroughMail(emailId, code) {
    console.log("Email", emailId);
    console.log("Code", code);
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `http://test.onlineexamkatta.com/getregDataNew.aspx/sendEmailForApk`,
      { EmailId: emailId, Code: code },
      {}
    );
  }

  changePassInputType() {
    if (this.passInputType == "password") {
      this.passInputType = "text";
      this.passIconName = "eye-off";
    } else {
      this.passInputType = "password";
      this.passIconName = "eye";
    }
  }

  changeReTypePassInputType() {
    if (this.reTypePassInputType == "password") {
      this.reTypePassInputType = "text";
      this.reTypePassIconName = "eye-off";
    } else {
      this.reTypePassInputType = "password";
      this.reTypePassIconName = "eye";
    }
  }
}
