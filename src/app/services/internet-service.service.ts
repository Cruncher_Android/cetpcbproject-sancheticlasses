import { Injectable } from '@angular/core';
import { Network } from "@ionic-native/network/ngx";

@Injectable({
  providedIn: 'root'
})
export class InternetServiceService {
  networkConnected: boolean;
  constructor(private network: Network) { 
    // console.log("in internet service");
    if(network.type=="none") 
      this.networkConnected = false;
    else 
      this.networkConnected = true;

      network.onConnect().subscribe(_ => this.networkConnected = true);
      network.onDisconnect().subscribe(_ => this.networkConnected = false)
  }
}
