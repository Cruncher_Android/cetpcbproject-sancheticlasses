import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class TestSeriesInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS testseries_info
        (subject_id integer primary key,sub_name varchar(255) ,phy_count integer,che_count integer,math_count integer,
        bio_count integer)`, []);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,phy_count)
        values(?,?,?)`,[1,'Physics',10]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,che_count)
        values(?,?,?)`,[2,'Chemistry',10]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,math_count)
        values(?,?,?)`,[3,'Mathematics',10]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,bio_count)
        values(?,?,?)`,[4,'Biology',10]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,phy_count,che_count,math_count)
        values(?,?,?,?,?)`,[5,'PCM',10,10,10]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testseries_info(subject_id,sub_name,phy_count,che_count,bio_count)
        values(?,?,?,?,?)`,[6,'PCB',10,10,10]);
      }
    });
  }
}
