import { UserInfo } from "./../../main/main.page";
import { UserInfoService } from "./../../services/user-info.service";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { Component, OnInit } from "@angular/core";
import { StorageServiceService } from "src/app/services/storage-service.service";
import { ActionSheetController, ToastController } from "@ionic/angular";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Crop } from "@ionic-native/crop/ngx";
import { File } from "@ionic-native/file/ngx";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  userInfo: UserInfo = {
    firstName: "",
    lastName: "",
    contactNo: "",
    email: "",
    birthDate: "",
    gender: "",
    parentsContactNo: "",
    standard: "",
    state: "",
    district: "",
    taluka: "",
    password: "",
    appId: "",
    appKey: "",
    deviceId: "",
    distributerId: "",
    expireDate: "",
    regiDate: "",
    status: "",
    activated: "",
    imageData: "",
  };
  imageURl: string = "";
  constructor(
    private camera: Camera,
    private actionSheetController: ActionSheetController,
    private toastController: ToastController,
    private databaseServiceService: DatabaseServiceService,
    private userInfoService: UserInfoService,
    private storageService: StorageServiceService,
    private crop: Crop,
    private file: File
  ) {}

  ngOnInit() {
    this.userInfo = this.storageService.userInfo;
    // this.databaseServiceService.getDatabaseState().subscribe(ready => {
    //   if(ready) {
    //     this.userInfoService.getUserInfo().then(result => {
    //       this.userInfo = result;
    //       console.log('profile', this.userInfo)
    //     })
    //   }
    // })
  }

  async onEditProfile() {
    const action = await this.actionSheetController.create({
      buttons: [
        {
          text: "Choose from gallary",
          handler: () => {
            this.chooseFile();
          },
        },
        {
          text: "Open Camera",
          handler: () => {
            this.openCamera();
          },
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    });
    await action.present();
  }

  chooseFile() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  cropImage(imageData) {
    this.crop.crop(imageData, { quality: 100 }).then((cropedImage) => {
      console.log("cropped image", cropedImage);
      let imagePath = cropedImage.split("?")[0];
      let copyPath = imagePath;
      let splitPath = copyPath.split("/");
      let imageName = splitPath[splitPath.length - 1];
      let filePath = imagePath.split(imageName)[0];
      console.log("image name", imageName);
      console.log("file path", filePath);

      this.file.readAsDataURL(filePath, imageName).then((dataUrl) => {
        console.log("data url", dataUrl);
        this.userInfo.imageData = dataUrl;
        this.storageService.userInfo.imageData = dataUrl;
        this.userInfoService
          .updateUserImage(dataUrl)
          .then((_) => this.createToast());
      });
    });
  }

  async createToast() {
    const toast = await this.toastController.create({
      animated: true,
      duration: 3000,
      message: "Profile picture updated successfully.",
    });
    await toast.present();
  }
}
