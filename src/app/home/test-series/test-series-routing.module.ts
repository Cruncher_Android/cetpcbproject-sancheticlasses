import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestSeriesPage } from './test-series.page';

const routes: Routes = [
  {
    path: '',
    component: TestSeriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestSeriesPageRoutingModule {}
