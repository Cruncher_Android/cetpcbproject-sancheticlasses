import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotesChaptersPageRoutingModule } from './notes-chapters-routing.module';

import { NotesChaptersPage } from './notes-chapters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotesChaptersPageRoutingModule
  ],
  declarations: [NotesChaptersPage]
})
export class NotesChaptersPageModule {}
