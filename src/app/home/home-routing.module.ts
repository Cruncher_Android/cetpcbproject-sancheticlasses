import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  // {
  //   path: 'notes-chapters',
  //   loadChildren: () => import('./notes-chapters/notes-chapters.module').then( m => m.NotesChaptersPageModule)
  // },
  // {
  //   path: 'notes',
  //   loadChildren: () => import('./notes/notes.module').then( m => m.NotesPageModule)
  // },
  // {
  //   path: 'profile',
  //   loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  // },
  // {
  //   path: 'unit-test',
  //   loadChildren: () => import('./unit-test/unit-test.module').then( m => m.UnitTestPageModule)
  // },
  //  {
  //   path: 'test-series',
  //  loadChildren: () => import('./test-series/test-series.module').then( m => m.TestSeriesPageModule)
  //  },
  // {
  //   path: 'bookmark-chapters',
  //   loadChildren: () => import('./bookmark-chapters/bookmark-chapters.module').then( m => m.BookmarkChaptersPageModule)
  // },
  // {
  //   path: 'question-paper',
  //   loadChildren: () => import('./question-paper/question-paper.module').then( m => m.QuestionPaperPageModule)
  // },
  // {
  //   path: 'career-notes',
  //   loadChildren: () => import('./career-notes/career-notes.module').then( m => m.CareerNotesPageModule)
  // },
  // {
  //   path: 'view-test',
  //   loadChildren: () => import('./view-test/view-test.module').then( m => m.ViewTestPageModule)
  // },
  // {
  //   path: 'bookmark',
  //   loadChildren: () => import('./bookmark/bookmark.module').then( m => m.BookmarkPageModule)
  // },
  // {
  //   path: 'about-us',
  //   loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  // },
  // {
  //   path: 'performance-graph',
  //   loadChildren: () => import('./performance-graph/performance-graph.module').then( m => m.PerformanceGraphPageModule)
  // },
  // {
  //   path: 'chapter-list/:subjectId',
  //   loadChildren: () => import('./chapter-list/chapter-list.module').then( m => m.ChapterListPageModule)
  // },
  // {
  //   path: 'chapter-list',
  //   loadChildren: () => import('./chapter-list/chapter-list.module').then( m => m.ChapterListPageModule)
  // },
  // {
  //   path: 'test',
  //   loadChildren: () => import('./test/test.module').then( m => m.TestPageModule)
  // },
  // {
  //   path: 'test/:chapterId',
  //   loadChildren: () => import('./test/test.module').then( m => m.TestPageModule)
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
